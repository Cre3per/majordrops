const config = require('./config.json');

const http = require('http');
const https = require('https');

const protocol = config.https ? https : http;
const protocolName = config.https ? 'https' : 'http';

/**
 * Time in milliseconds since 1970 when the last request has been responded to successfully
 * @type{number}
 */
let lastSuccessfulRequest = 0;


/**
 * Keeps calling itself every config.hearbeatinterval millisecond(s) and sends
 * https requests for each viewer to each stream as defined in config.viewers
 */
function idle()
{
  for (let channel in config.channels)
  {
    for (let viewer of config.channels[channel])
    {
      let request = protocol.get(`${protocolName}://api.twitch.tv/steam/watching?channel=${channel}&viewer=${viewer}`, (res) =>
      {
        // Twitch sends an empty response on success.
        // We only need to check for NO CONTENT
        if (res.statusCode === 204)
        {
          lastSuccessfulRequest = Date.now();
        }
        else
        {
          console.log(`Ожибка: ${res.statusCode}`);
        }
      });

      request.on('error', (err) =>
      {
        console.log(`Ожибка при росмотре ${channel} как ${viewer}`);
        console.log(err);
      });
    }
  }

  let now = Date.now();

  if ((now - lastSuccessfulRequest) > (config.hearbeatinterval * 2))
  {
    console.log(`API не сети с ${Math.floor((now - lastSuccessfulRequest) / 1000.0)} секунд(ы)`);
  }

  setTimeout(idle, config.hearbeatinterval);
}

/**
 * Entry point
 */
function main()
{
  console.log('Привет!');
  console.log(`Протокол: ${protocolName}`);

  let channelIds = Object.keys(config.channels);
  
  let channelCount = channelIds.length;
  let viewerCount = 0;

  for (let i = 0; i < channelCount; ++i)
  {
    for (let viewer of config.channels[channelIds[i]])
    {
      let isNewViewer = true;

      for (let j = 0; j < i; ++j)
      {
        if (config.channels[channelIds[j]].includes(viewer))
        {
          isNewViewer = false;
          break;
        }
      }

      if (isNewViewer)
      {
        ++viewerCount;
      }
    }
  }

  console.log(`${viewerCount} зрителей смотрят ${channelCount} пряамом эфире`);

  lastSuccessfulRequest = Date.now();

  idle();
}


main();